shaft_diameter = 10;
shaft_length = 20;

cap_diameter = 20;
cap_thickness = 2;

$fs = $preview ? 3 : 0.5;
$fa = $preview ? 5 : 1;

cylinder(d = cap_diameter, h = cap_thickness);
translate([ 0, 0, cap_thickness ])
  cylinder(d = shaft_diameter, h = shaft_length);
